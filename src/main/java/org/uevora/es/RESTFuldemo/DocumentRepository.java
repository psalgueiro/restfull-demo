package org.uevora.es.RESTFuldemo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface DocumentRepository extends CrudRepository<Document, Integer> {

	Document findById(int id);
	List<Document> findByTitle(String title);
	void deleteById(int id);

}