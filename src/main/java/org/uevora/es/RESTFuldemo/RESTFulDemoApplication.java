package org.uevora.es.RESTFuldemo;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RESTFulDemoApplication {

	private static final Logger log = LoggerFactory.getLogger(RESTFulDemoApplication.class);
	  
	public static void main(String[] args) {
		SpringApplication.run(RESTFulDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(DocumentRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new Document("title 1", "author 1"));
			repository.save(new Document("title 2", "author 2"));

			// fetch all customers
			log.info("Documents found with findAll():");
			log.info("-------------------------------");
			for (Document document : repository.findAll()) {
				log.info(document.toString());
			}
			log.info("");

			// fetch an individual document by ID
			Document document = repository.findById(1);
			log.info("Customer found with findById(1):");
			log.info("--------------------------------");
			log.info(document.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByTitle('title 1'):");
			log.info("--------------------------------------------");
			repository.findByTitle("Bauer").forEach(bauer -> {
				log.info(bauer.toString());
			});

			log.info("");
		};
	}
	  
}
