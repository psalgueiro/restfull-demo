package org.uevora.es.RESTFuldemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;


@RestController
public class DocoumentController {
	
	@Autowired
	DocumentRepository documentRepository;
	
	// http://localhost:8080/getDocument/1
	@GetMapping("/getDocument/{id}")
	public Document getDocument(@PathVariable Integer id){
		
		return documentRepository.findById(id)
				 .orElseThrow(() -> {	    	  
			    	  throw new ResponseStatusException(
			    			  HttpStatus.NOT_FOUND, 
			    			  String.format("Document with id=%d not found",id)
			    			  );	   
			      });
	}
	
	
	// http://localhost:8080/getDocuments
	@GetMapping("/getDocuments")
	public Iterable<Document> getDocuments(){
		
		return documentRepository.findAll();
	}
	
	// http://localhost:8080/newDocument
	@PostMapping("/newDocument")
	public Document newDocument(@RequestBody Document document){
		return documentRepository.save(document);
	}
	
	// http://localhost:8080/updateDocument
	@PutMapping("/updateDocument/{id}")
	public Document updateDocument(@RequestBody Document newDocument, @PathVariable Integer id) {
	    
	    return documentRepository.findById(id)
	      .map(document -> {
	        document.setTitle(newDocument.getTitle());
	        document.setAuthor(newDocument.getAuthor());
	        return documentRepository.save(document);
	      })
	      .orElseGet(() -> {	    	  
	    	  throw new ResponseStatusException(
	    			  HttpStatus.NOT_FOUND, 
	    			  String.format("Document with id=%d not found",id)
	    			  );	   
	      });
	  }
	
	  // http://localhost:8080/deleteDocument
	  @DeleteMapping("/deleteDocument/{id}")
	  void deleteEmployee(@PathVariable Integer id) {
		  documentRepository.deleteById(id);
	  }
	
}
